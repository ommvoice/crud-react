export interface Contact {
    id?: number;
    name: string;
    email: string;
    created?: string;
    modified?: string
}
  
export interface Contacts {
    contacts: Contact[];
}