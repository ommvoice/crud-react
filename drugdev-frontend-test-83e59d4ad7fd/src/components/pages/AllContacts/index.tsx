import * as React from 'react';
import AddContact from '../../../components/AddContact';
import ListContacts from '../../../components/ListContacts';

const AllContacts = () => {
    return <>
            <div style={{paddingTop: 20}}>Contacts</div>
            <AddContact />
            <ListContacts />
           </>
  };
  
  export default AllContacts;