import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import EditContact from '../../EditContact'

export default class EditContactPage extends React.Component<RouteComponentProps<any>> {
  render() {
    const param =  this.props.match.params.id;

    return (
        <EditContact id={param} />
    );
  }
}