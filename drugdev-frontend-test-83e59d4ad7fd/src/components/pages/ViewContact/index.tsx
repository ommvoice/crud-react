import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import ViewContact from '../../ViewContact'

export default class ViewontactPage extends React.Component<RouteComponentProps<any>> {
  render() {
    const param =  this.props.match.params.id;

    return (
        <ViewContact id={param} />
    );
  }
}