import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import EditContact from '../EditContact';
import ViewContact from '../ViewContact';
import AllContacts from '../AllContacts';

const Main = () => (
  <Router>
    <Route exact path={["/", "/contacts"]} component={AllContacts} />
    <Route path="/edit/:id" name="edit" component={EditContact} />
    <Route path="/contact/:id" name="viewcontact" component={ViewContact} />
  </Router>
);

export default Main;
