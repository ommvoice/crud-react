import React, { useState }from 'react';
import { Contact } from '../../types';
import { addContact } from './graphql/mutations';

import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

const AddContact: React.FC = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const data: Contact = {
        name,
        email
    };

  const addContactMutation  = addContact(data);
  const addContactHandler = function() {
    addContactMutation();
    window.location.href = "/contacts";
  };

  return (
        <form
            onSubmit={e => {
                e.preventDefault();

                if (name && email) {
                addContactHandler();
                setName('')
                setEmail('')
                }
            }}
            >
            <Grid container>
                <Grid item xs={12}>
                <Grid
                    container
                    spacing={16}
                    alignItems='center'
                    direction='row'
                    justify='center'
                >
                    <Grid item>
                    <TextField
                        id="name"
                        label="Name"
                        margin="normal"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />
                    </Grid>
                    <Grid item>
                    <TextField
                        id="email"
                        label="Email"
                        margin="normal"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                    </Grid>
                    <Grid item>
                    <Button
                        variant="outlined"
                        color="secondary"
                        size="small"
                        type="submit"
                    >
                        Add Contact
                    </Button>
                    </Grid>
                </Grid>
                </Grid>
            </Grid>
        </form>
  )}

export default AddContact;