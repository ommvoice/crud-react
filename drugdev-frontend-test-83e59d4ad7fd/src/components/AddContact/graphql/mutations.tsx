import gql from "graphql-tag";
import  { useMutation } from "react-apollo-hooks";
import { Contact } from '../../../types'

const CONTACT_ADD = gql`
mutation AddContact($contact: InputContact) {
  addContact(contact : $contact) {
      id
      name
      email
  }
}
`;

export function addContact(contact: Contact) {
    return useMutation(CONTACT_ADD, {
    variables: { contact }
  });
}