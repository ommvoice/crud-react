import * as React from 'react';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { Contact, Contacts } from '../../types';
import DeleteContact from '../DeleteContact/DeleteContact';
import { Link } from 'react-router-dom';
import "./ListContacts.css";

interface Props  {
  data: Contacts;
}

interface ContactProps  {
  contact: Contact;
}

const ContactItem: React.FC<ContactProps> = ({ contact }) => (
  <ListItem>
    <ListItemText 
      primary={<Typography style={{ color: '#fff' }}>{contact.name}</Typography>}
      />
    <ListItemText 
      primary={<Typography style={{ color: '#fff' }}>{contact.email}</Typography>}
      />
    <ListItemSecondaryAction>
        <div style={{width: 300, color: 'white' }}>
          {<DeleteContact id={contact.id} />}
          &nbsp; &nbsp;
          <Link className="contacts__nostyle" to={'contact/'+ contact.id }>
              View
          </Link>

          &nbsp; &nbsp;
          <Link className="contacts__nostyle" to={'edit/'+ contact.id }>
              Edit
          </Link>
        </div>

    </ListItemSecondaryAction>
  </ListItem>
);

const ListContacts: React.FC<Props> = ({ data }) => (
  <Grid container justify="center" style={{ marginTop: "3em" }}>
        <Grid
          container
          alignItems="center"
          justify="center"
        >
          <Grid item>
            <List style={{ border: "solid 1px grey", width: 800 }}>
              {!!data.contacts &&
                  data.contacts.map(contact => (
                    <ContactItem contact={contact} key={contact.id} />
                ))}
            </List>
          </Grid>
        </Grid>
      </Grid>
);

export default ListContacts;