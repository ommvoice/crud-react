import gql from "graphql-tag";
import  { useQuery } from "react-apollo-hooks";
import { Contacts } from '../../../types'

export const QUERY_CONTACT_LIST = gql`
  query ListContacts {
    contacts {
      id
      name
      email
    }
  }
`;

interface ListContactQueryVariables {}
export function getContacts() {
  return useQuery<Contacts, ListContactQueryVariables>(
    QUERY_CONTACT_LIST
  );
} 