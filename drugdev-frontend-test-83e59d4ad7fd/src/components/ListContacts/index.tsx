import * as React from 'react';
import { getContacts } from './graphql/query';
import ListContacts from './ListContacts';

const ListContactsContainer = () => {
    const { data, error, loading } = getContacts();
  
    if (loading) {
      return <div>Loading...</div>;
    }
  
    if (error || !data) {
      return <div>ERROR</div>;
    }
  
    return <ListContacts data={data} />;
  };
  
  export default ListContactsContainer;