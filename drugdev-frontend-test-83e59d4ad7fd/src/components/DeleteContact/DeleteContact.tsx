import React, { useState }from 'react';
import { deleteContact } from './graphql/mutations';
import CloseIcon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";

interface Props {
  id?: number;
}

const DeleteContact = ({id}: Props) => {
    const deleteContactMutation  = deleteContact(id);
    const deleteContactHandler = function() {
        deleteContactMutation();
        window.location.href = "/contacts";
    };

    return (
        <IconButton onClick={() => deleteContactHandler() }>
            <CloseIcon style={{color: 'red'}}
            > x 
            </CloseIcon>
         </IconButton>
    )
}

export default DeleteContact;