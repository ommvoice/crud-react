import gql from "graphql-tag";
import  { useMutation } from "react-apollo-hooks";

const CONTACT_DELTE = gql`
mutation DeleteContact($id: ID!) {
    deleteContact(id: $id) 
}
`;

export function deleteContact(id?: number) {
    return useMutation(CONTACT_DELTE, {
    variables: { id }
  });
}