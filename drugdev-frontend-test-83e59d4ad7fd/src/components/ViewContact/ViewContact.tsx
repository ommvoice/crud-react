import React from 'react';
import { Contact } from '../../types';
import Grid from "@material-ui/core/Grid";

interface Props {
  contact: Contact;
}

const ViewContact = ({contact}: Props) => {

return (
    <Grid container alignContent="center" direction="column">
            <div style={{display: "flex"}}><b>Name:</b> &nbsp;&nbsp;{contact.name}</div>
            <div style={{display: "flex"}}><b>Email:</b> &nbsp; &nbsp;{contact.email}</div>
            <div><b>Created: </b> &nbsp; &nbsp;{contact.created}</div>
            <div><b>Modified:</b> &nbsp; &nbsp;{contact.modified}</div>
    </Grid>
)}

export default ViewContact;