import * as React from 'react';
import EditContact from './EditContact';
import { getContact } from './graphql/query';
import { Link } from 'react-router-dom';

interface Props {
    id?: string;
    }
 
const EditContactContainer = ({id}: Props) => {
    if(id) {
       const { data } = getContact(id);

        if(data && data.contact) {
    
        return <div>
                 <EditContact contact={data.contact} />
                 <div style={{paddingTop: 50}}>
                    <Link className="contacts__nostyle" to={'/contacts'}>
                        Go Back
                    </Link>
                </div>
            </div>;
        }
    }

    return null;
};

export default EditContactContainer;