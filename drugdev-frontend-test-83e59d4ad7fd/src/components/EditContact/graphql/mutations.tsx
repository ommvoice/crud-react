import gql from "graphql-tag";
import  { useMutation } from "react-apollo-hooks";
import { Contact } from '../../../types'

const CONTACT_EDIT = gql`
  mutation UpdateContact($contact: InputContact) {
    updateContact(contact : $contact) {
        id
        name
        email
    }
}
`;

export function editContact(contact: Contact) {
    return useMutation(CONTACT_EDIT, {
    variables: { contact }
  });
}