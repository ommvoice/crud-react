import gql from "graphql-tag";
import  { useQuery } from "react-apollo-hooks";
import { Contact } from '../../../types'

export const QUERY_CONTACT = gql`
  query GetContact($id: ID) {
    contact(id: $id) {
      id
      name
      email
    }
  }
`;

interface ListContactQueryVariables {
    id: string
}

interface ContactDetail {
    contact: Contact
}

export function getContact(id: string) {
  return useQuery<ContactDetail, ListContactQueryVariables>(
    QUERY_CONTACT,{
        variables: { id }
    }
  );
} 