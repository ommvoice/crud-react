import React, { useState }from 'react';
import { Contact } from '../../types';
import { editContact } from './graphql/mutations';

import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

interface Props {
  contact: Contact;
}

const EditContact = ({contact}: Props) => {
    const [name, setName] = useState(contact.name);
    const [email, setEmail] = useState(contact.email);
 
    const data: Contact = {
        id: contact.id,
        name,
        email
    };

const editontactMutation  = editContact(data);
const editContactHandler = function() {
    editontactMutation();
};

return (
    <form
        onSubmit={e => {
            e.preventDefault();

            if (name && email) {
                editContactHandler();
            }
        }}
        >
        <Grid container>
            <Grid item xs={12}>
            <Grid
                container
                spacing={16}
                alignItems='center'
                direction='row'
                justify='center'
            >
                <Grid item>
                <TextField
                    id="name"
                    label="Name"
                    margin="normal"
                    value={name}
                    onChange={e => setName(e.target.value)}
                />
                </Grid>
                <Grid item>
                <TextField
                    id="email"
                    label="Email"
                    margin="normal"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
                </Grid>
                <Grid item>
                <Button
                    variant="outlined"
                    color="secondary"
                    size="small"
                    type="submit"
                >
                    Update Contact
                </Button>
                </Grid>
            </Grid>
            </Grid>
        </Grid>
    </form>
)}

export default EditContact;